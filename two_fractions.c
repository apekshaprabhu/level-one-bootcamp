//WAP to find the sum of two fractions.
#include <stdio.h>
typedef struct
{
    int numerator;
    int denominator;
}Fraction;
Fraction add(Fraction a, Fraction b)
{
    Fraction c;
    c.denominator = a.denominator * b.denominator;
    c.numerator = (a.numerator * b.denominator) + (b.numerator * a.denominator);
    c = simplify(c);
    return c;
}
int get_gcd(int a, int b)
        {
                    int temp;
                while (a!=0)
                {
                            temp=a;
                            a=b%a;
                            b=temp;
                }
        return b;
        }
Fraction simplify(Fraction add)
{
    int gcd=get_gcd(add.numerator,add.denominator);
    add.numerator=add.numerator/gcd;
    add.denominator=add.denominator/gcd;
    return add;
}
Fraction input()
    {
     Fraction a;
     printf("Enter fraction: numerator/denominator:");
     scanf("%d/%d", &a.numerator, &a.denominator);
     return a;
    }
    void output(Fraction a, Fraction b, Fraction result)
    {
        printf("%d/%d + %d/%d =%d/%d", a.numerator, a.denominator, b.numerator, b.denominator, result.numerator,result.denominator); 
    }
int main()
{
     Fraction a= input();
     Fraction b =input();
     Fraction result = add(a, b);
     output(a, b, result);
     return 0;
    }