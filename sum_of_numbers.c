//Write a program to find the sum of n different numbers using 4 functions
#include<stdio.h>
int input()
{
	int n;
	printf("Enter the value for n:");
	scanf("%d",&n);
	return n;
}
int add(int number)
{
   int sum = 0;
   for(int i=1; i<=number; i++)
   {
     sum += i;
   }
   return sum;
}
void display(int n, int add)
{
   printf("1+2+3+….+%d+%d = %d",n-1, n, add);
}

int main()
{
   int range, result;
   range = input();
   result =add(range);
   display(range, result);
   return 0;
}

