//WAP to find the distance between two points using structures and 4 functions.
#include<stdio.h>
#include<math.h>
typedef struct 
{
	float x;
	float y;
}POINTS;
float dist(POINTS m, POINTS n)
{
	return sqrt(pow((n.x - m.x), 2) + pow((n.y - m.y), 2));
}
POINTS input()
{
POINTS m;
printf("Enter the coordinate points x and y ");
scanf("%f%f",&m.x, &m.y);
return m;
}
void display(float d)
{
	printf("the distance between the points is %f\n",d);
}
int main()
{
    float distance;
    POINTS m,n;
    m = input();
    n = input();
    distance = dist(m,n);
    display(distance);
    return 0;
}
