//WAP to find the sum of n fractions.
#include<stdio.h>
	typedef struct
	{
	    int n;
	    int d;
	}FRACTION;
int input()
{
	int n;
	printf("Enter the number of fractions to be added");
	scanf("%d",&n);
	return n;
}
void get_frac(int n,FRACTION array[n])
{
	FRACTION x;
	for(int i=0;i<n;i++)
{
	printf("Enter the numerator of fraction");
	scanf("%d",&x.n);
	printf("Enter the denominator of fraction");
	scanf("%d",&x.d);
	array[i]=x;
}
}
int gcd(int a, int b)
{
	int temp;
	while(a!=0)
	{
	temp=a;
	a=b%a;
	b=temp;
	}
}
FRACTION reduce(FRACTION sum)
{
int GCD;
GCD=gcd(sum.n,sum.d);
sum.n=sum.n/GCD;
sum.d=sum.d/GCD;
return sum;
}
FRACTION sum_of_two(FRACTION f1,FRACTION f2)
{
	FRACTION sum;
	sum.n=(f1.n*f2.d+f2.n*f1.d);
	sum.d=(f1.d*f2.d);
	sum=reduce(sum);
	return sum;
}
FRACTION total_sum(int n,FRACTION array[n])
{
	FRACTION sum;
	sum=array[0];
	for(int i=0;i<n;i++)
	{
		sum=sum_of_two(sum,array[i]);
	}
	return sum;
}
void display(int n, FRACTION array[n],FRACTION sum)
{
	printf("the sum is:");
	for(int i=0;i<n-1;i++)
	{
		printf("%d/%d + ",array[i].n,array[i].d);
	}
printf("%d/%d is :%d/%d\n",array[n-1].n,array[n-1].d,sum.n,sum.d);
}
int main()
{
int n;
n=input();
FRACTION array[n],sum;
get_frac(n,array);
sum=total_sum(n,array);
display(n,array,sum);
return 0;
}